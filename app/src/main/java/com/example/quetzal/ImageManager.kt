package com.example.quetzal

import android.app.Application
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.annotation.NonNull
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import java.io.File

/**
 *Created by Andrés Rodríguez on 06/11/2020.
 */
class ImageManager {
    private val options = RequestOptions()
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .override(Target.SIZE_ORIGINAL)
        .dontTransform()

    private lateinit var thumbRequest: RequestBuilder<Drawable>
    private lateinit var errorRequest: RequestBuilder<Drawable>

    companion object {
        var imageManager = ImageManager()
    }

    fun initialize(@NonNull application: Application) {
        this.errorRequest = Glide.with(application.applicationContext)
            .load(R.drawable.ic_error)
            .apply(options)

        this.thumbRequest = Glide.with(application.applicationContext)
            .load(R.drawable.ic_loading)
            .apply(options)
    }

    fun setImage(@NonNull url: String, @NonNull image: ImageView) {
        Glide.with(image)
            .load(url)
            .apply(options)
            .thumbnail(thumbRequest)
            .error(errorRequest)
            .into(image)
    }

    fun setImage(@NonNull file: File, @NonNull image: ImageView) {
        Glide.with(image)
            .load(file)
            .apply(options)
            .thumbnail(thumbRequest)
            .error(errorRequest)
            .into(image)

    }
}