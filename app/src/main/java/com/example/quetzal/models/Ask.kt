package com.example.quetzal.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.quetzal.room.RoomConstants

@Entity(tableName = RoomConstants.tableAsk)
class Ask(
    @ColumnInfo(name = RoomConstants.columnAskId)
    @PrimaryKey
    val askId: Int,
    @ColumnInfo(name = RoomConstants.columnAskName)
    val askName: String,
    @ColumnInfo(name = RoomConstants.columnAskCategory)
    val askCategory: String
)