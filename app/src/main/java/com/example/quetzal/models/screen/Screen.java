package com.example.quetzal.models.screen;

import androidx.annotation.NonNull;

public class Screen {
    @NonNull
    private final String TAG;

    private final boolean networkRequired; // FIXME: 09/09/2020 checck if in a future we're going to use this
    /**
     * Constructor for activities, fragments and dialogs
     * */
    Screen(boolean networkRequired, @NonNull String TAG) {
        this.networkRequired = networkRequired;
        this.TAG = TAG;
    }

    public boolean isNetworkRequired() {
        return networkRequired;
    }

    public String getTAG() {
        return TAG;
    }
}