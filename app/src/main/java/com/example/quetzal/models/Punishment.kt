package com.example.quetzal.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.quetzal.room.RoomConstants

@Entity(tableName = RoomConstants.tablePunishment)
class Punishment(
    @ColumnInfo(name = RoomConstants.columnPunishmentId)
    @PrimaryKey
    val punishmentId: Int,
    @ColumnInfo(name = RoomConstants.columnPunishmentName)
    val punishmentName: String,
    @ColumnInfo(name = RoomConstants.columnPunishmentCategory)
    val punishmentCategory: String,
    @ColumnInfo(name = RoomConstants.columnPunishmentPicture)
    val punishmentPicture: String,
    @ColumnInfo(name = RoomConstants.getColumnPunishmentPictureUrl)
    val punishmentPictureUrl: String
)