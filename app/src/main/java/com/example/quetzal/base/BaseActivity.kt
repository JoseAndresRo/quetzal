package com.example.quetzal.base

import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewbinding.ViewBinding
import com.example.quetzal.R
import com.example.quetzal.models.screen.Screen
import com.example.quetzal.utils.LogUtils
import com.google.android.material.appbar.AppBarLayout

abstract class BaseActivity : AppCompatActivity() {
    protected var tvTitle: TextView? = null
    protected var appBarLayout: AppBarLayout? = null
    protected var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        init()
        prepareComponents()
    }

    protected abstract val screen: Screen?
    protected abstract val viewBinding: ViewBinding
    protected abstract fun prepareComponents()
    private fun init() {
        appBarLayout = findViewById(R.id.appBarLayout)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    fun showToast(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    protected fun setTitle(title: String?) {
        if (tvTitle != null) {
            tvTitle!!.text = title
        }
    }

    protected fun toolbarVisibility(visibility: Boolean) {
        if (appBarLayout != null) {
            if (visibility) {
                visible(appBarLayout)
            } else {
                gone(appBarLayout)
            }
        }
    }

    protected fun visible(view: View?) {
        if (view == null) {
            LogUtils.print("View are null (Visible - BaseActivity)")
            return
        }
        if (view.visibility != View.VISIBLE) {
            view.visibility = View.VISIBLE
        }
    }

    protected fun gone(view: View?) {
        if (view == null) {
            LogUtils.print("View are null (Gone - BaseActivity)")
            return
        }
        if (view.visibility != View.GONE) {
            view.visibility = View.GONE
        }
    }
}