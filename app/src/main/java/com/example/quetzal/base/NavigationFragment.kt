package com.example.quetzal.base

import android.os.Bundle
import androidx.navigation.Navigation
import com.example.quetzal.R
import com.example.quetzal.databinding.*
import com.example.quetzal.utils.Constants

/**
 * Created by Andrés Rodríguez on 14/10/2020.
 */
abstract class NavigationFragment : BaseFragment() {
    private fun setOptions(bundle: Bundle, option1: String?, option2: String?) {
        bundle.putString(Constants.OPTION_1, option1)
        bundle.putString(Constants.OPTION_2, option2)
    }

    protected val option1: String?
        get() = if (arguments != null) {
            requireArguments().getString(Constants.OPTION_1)
        } else "empty"
    protected val option2: String?
        get() = if (arguments != null) {
            requireArguments().getString(Constants.OPTION_2)
        } else "empty"

    protected fun openPunishment1(option1: String?, option2: String?, binding: FragmentAsk1Binding) {
        val bundle = Bundle()
        setOptions(bundle, option1, option2)
        Navigation.findNavController(binding.root).navigate(R.id.action_ask1Fragment_to_punishment1Fragment, bundle)
    }

    protected fun openAsk2(binding: FragmentPunishment1Binding) {
        Navigation.findNavController(binding.root).navigate(R.id.action_punishment1Fragment_to_ask2Fragment)
    }

    protected fun openPunishment2(option1: String?, option2: String?, binding: FragmentAsk2Binding) {
        val bundle = Bundle()
        setOptions(bundle, option1, option2)
        Navigation.findNavController(binding.root).navigate(R.id.action_ask2Fragment_to_punishment2Fragment, bundle)
    }

    protected fun openAsk3(binding: FragmentPunishment2Binding) {
        Navigation.findNavController(binding.root).navigate(R.id.action_punishment2Fragment_to_ask3Fragment)
    }

    protected fun openPunishment3(option1: String?, option2: String?, binding: FragmentAsk3Binding) {
        val bundle = Bundle()
        setOptions(bundle, option1, option2)
        Navigation.findNavController(binding.root).navigate(R.id.action_ask3Fragment_to_punishment3Fragment, bundle)
    }

    protected fun openResult(binding: FragmentPunishment3Binding) {
        Navigation.findNavController(binding.root).navigate(R.id.action_punishment3Fragment_to_resultFragment)
    }
}