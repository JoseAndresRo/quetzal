package com.example.quetzal.base

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.quetzal.models.screen.Screen
import com.example.quetzal.utils.LogUtils

abstract class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        LogUtils.print("FragmentName: " + screen.tag)
        prepareComponents()
    }

    protected abstract val screen: Screen
    protected abstract fun prepareComponents()

    private val viewActivity: FragmentActivity
        get() = requireActivity()

    fun showToast(message: String?) {
        Toast.makeText(viewActivity, message, Toast.LENGTH_SHORT).show()
    }
}