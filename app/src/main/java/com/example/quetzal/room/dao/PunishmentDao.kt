package com.example.quetzal.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.quetzal.models.Punishment

@Dao
interface PunishmentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(punishment: Punishment?)

    @Update
    fun update(punishment: Punishment?)

    @Delete
    fun delete(punishment: Punishment?)

    @Query("DELETE FROM tablePunishment")
    fun deleteAll()

    @Query("SELECT * FROM tablePunishment WHERE columnPunishmentId= :punishmentId")
    fun getPunishmentById(punishmentId: Int): Punishment?

    @Query("SELECT * FROM tablePunishment WHERE columnPunishmentCategory = :punishmentCategory")
    fun getPunishmentsByCategory(punishmentCategory: String?): List<Punishment?>?

    @Query("SELECT * FROM tablePunishment ORDER BY columnPunishmentId DESC")
    fun allPunishment(): LiveData<List<Punishment?>?>?

    @Query("SELECT * FROM tablePunishment WHERE columnPunishmentCategory = :punishmentCategory ORDER BY RANDOM() LIMIT 1")
    fun getRandomPunishmentsByCategory(punishmentCategory: String?): Punishment?
}