package com.example.quetzal.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.quetzal.asynctasks.PopulateDbAsyncTask
import com.example.quetzal.models.Ask
import com.example.quetzal.models.Punishment
import com.example.quetzal.room.dao.AskDao
import com.example.quetzal.room.dao.PunishmentDao

@Database(entities = [Ask::class, Punishment::class], version = RoomConstants.databaseVersion, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun askDao(): AskDao
    abstract fun punishmentDao(): PunishmentDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    RoomConstants.databaseName
                )
                    .addCallback(roomCallback)
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private val roomCallback: Callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                PopulateDbAsyncTask(INSTANCE).execute()
            }
        }
    }
}