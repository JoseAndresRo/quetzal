package com.example.quetzal.room

object RoomConstants {
    const val databaseName = "Quetzal.db"
    const val databaseVersion = 1

    //Table Names
    const val tableAsk = "tableAsk"
    const val tablePunishment = "tablePunishment"

    //Table Ask
    const val columnAskId = "columnAskId"
    const val columnAskName = "columnAskName"
    const val columnAskCategory = "columnAskCategory"

    //Table Punishment
    const val columnPunishmentId = "columnPunishmentId"
    const val columnPunishmentName = "columnPunishmentName"
    const val columnPunishmentCategory = "columnPunishmentCategory"
    const val columnPunishmentPicture = "columnPunishmentPicture"
    const val getColumnPunishmentPictureUrl = "columnPunishmentPictureUrl"
}