package com.example.quetzal.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.quetzal.models.Ask

@Dao
interface AskDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ask: Ask)

    @Update
    fun update(ask: Ask)

    @Delete
    fun delete(ask: Ask)

    @Query("DELETE FROM tableAsk")
    fun deleteAll()

    @Query("SELECT * FROM tableAsk WHERE columnAskId= :askId LIMIT 1")
    fun getAskById(askId: Int): Ask

    @Query("SELECT * FROM tableAsk WHERE columnAskCategory = :askCategory")
    fun getAsksByCategory(askCategory: String): LiveData<List<Ask>>

    @Query("SELECT * FROM tableAsk ORDER BY columnAskId")
    fun allAsks(): LiveData<List<Ask>>
}