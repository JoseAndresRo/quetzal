package com.example.quetzal.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.quetzal.ImageManager.Companion.imageManager
import com.example.quetzal.base.NavigationFragment
import com.example.quetzal.databinding.FragmentPunishment3Binding
import com.example.quetzal.models.Punishment
import com.example.quetzal.models.screen.FragmentScreen
import com.example.quetzal.models.screen.Screen
import com.example.quetzal.utils.Constants.CATEGORY_C
import com.example.quetzal.utils.LogUtils
import com.example.quetzal.viewmodels.PunishmentViewModel
import com.example.quetzal.viewmodels.ResultViewModel

class Punishment3Fragment : NavigationFragment() {
    override val screen: Screen = FragmentScreen(false, "Punishment 3")
    private var _binding: FragmentPunishment3Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentPunishment3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun prepareComponents() {
        val option1 = option1!!
        val option2 = option2!!
        val txPunishment = arrayOf("")
        LogUtils.print("Options: $option1 $option2")

        val viewModel: PunishmentViewModel = ViewModelProvider(this).get(PunishmentViewModel::class.java)
        val viewModelResult = ViewModelProvider(this).get(ResultViewModel::class.java)

        binding.btnNextQuestion.setOnClickListener {
            openResult(binding)
            viewModelResult.generalResult(option1, option2, txPunishment[0], CATEGORY_C)
        }
        viewModel.punishment.observe(this, { punishment: Punishment? ->
            if (punishment != null) {
                txPunishment[0] = punishment.punishmentName
                val pictureUrl = punishment.punishmentPictureUrl
                binding.txPunishment3.text = txPunishment[0]
                LogUtils.print("PictureUrl: $pictureUrl")

                imageManager.setImage(pictureUrl, binding.ivPunishment1)
            }
        })
        viewModel.getPunishmentCategory(CATEGORY_C)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}