package com.example.quetzal.ui.fragments

import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.quetzal.R
import com.example.quetzal.base.NavigationFragment
import com.example.quetzal.databinding.FragmentResultBinding
import com.example.quetzal.models.screen.FragmentScreen
import com.example.quetzal.models.screen.Screen
import com.example.quetzal.ui.activities.ContainerActivity
import com.example.quetzal.utils.LogUtils
import com.example.quetzal.viewmodels.ResultViewModel
import nl.dionsegijn.konfetti.models.Shape.Circle
import nl.dionsegijn.konfetti.models.Shape.Square
import nl.dionsegijn.konfetti.models.Size

class ResultFragment : NavigationFragment() {
    override val screen: Screen = FragmentScreen(false, "Result Fragment")
    private var _binding: FragmentResultBinding? = null
    private val binding get() = _binding!!
    private var mediaPlayer: MediaPlayer? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentResultBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        if (mediaPlayer!!.isPlaying) {
            mediaPlayer!!.pause()
        }
    }

    override fun onResume() {
        super.onResume()
        mediaPlayer!!.start()
    }

    override fun prepareComponents() {
        //Set Confetti
        binding.konfettiView.build()
            .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
            .setDirection(0.0, 359.0)
            .setSpeed(1f, 5f)
            .setFadeOutEnabled(true)
            .setTimeToLive(50000L)
            .addShapes(Square, Circle)
            .addSizes(Size(12, 5f))
            .setPosition(-50f, binding.konfettiView.width + 50f, -50f, -50f)
            .streamFor(300, 5000L)

        //Set music
        mediaPlayer = MediaPlayer.create(context, R.raw.sonidomarchanubcial)
        mediaPlayer!!.start()
        val viewModel: ResultViewModel = ViewModelProvider(this).get(ResultViewModel::class.java)

        binding.btnOkay.setOnClickListener {
            val intent = Intent(context, ContainerActivity::class.java)
            startActivity(intent)
            requireActivity().finish()
        }

        viewModel.result1.observe(this, { result: String ->
            LogUtils.print("DATA_RECEIVED: $result")
            binding.txResult1.text = result
        })

        viewModel.result2.observe(this, { result: String? -> binding.txResult2.text = result })
        viewModel.result3.observe(this, { result: String ->
            LogUtils.print("DATA_FROM_GENERAL_RESULT$result")
            binding.txResult3.text = result
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}