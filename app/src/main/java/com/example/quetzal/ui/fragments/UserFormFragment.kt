package com.example.quetzal.ui.fragments

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.quetzal.R
import com.example.quetzal.base.BaseFragment
import com.example.quetzal.databinding.FragmentUserFormBinding
import com.example.quetzal.models.screen.FragmentScreen
import com.example.quetzal.models.screen.Screen
import com.example.quetzal.ui.activities.MainActivity

class UserFormFragment : BaseFragment() {
    override val screen: Screen = FragmentScreen(false, "UserFormFragment")
    private var _binding: FragmentUserFormBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentUserFormBinding.inflate(inflater, container, false)
        //Animation button play
        val animationBtnPlay = binding.btnOkay.background as AnimationDrawable
        animationBtnPlay.setEnterFadeDuration(1500)
        animationBtnPlay.setExitFadeDuration(1500)
        //Animation button back
        val animationBtnBack = binding.btnExit.background as AnimationDrawable
        animationBtnBack.setEnterFadeDuration(1500)
        animationBtnBack.setExitFadeDuration(1500)
        return binding.root
    }

    override fun prepareComponents() {
        binding.btnOkay.setOnClickListener { nextActivityQuestion() }
        binding.btnExit.setOnClickListener {
            val intent = Intent(context, MainActivity::class.java)
            startActivity(intent)
            requireActivity().finish()
        }
    }

    private fun nextActivityQuestion() {
        val name = binding.etName.text.toString()
        val edge = binding.etAge.text.toString()
        when {
            name.isEmpty() -> {
                showToast("Name is empty")
                binding.etName.isFocusable = true
            }
            edge.isEmpty() -> {
                showToast("Edge is empty")
                binding.etAge.isFocusable = true
            }
            else -> {
                Navigation.findNavController(binding.root)
                    .navigate(R.id.action_userFormFragment_to_ask1Fragment)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}