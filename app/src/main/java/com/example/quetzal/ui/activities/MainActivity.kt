package com.example.quetzal.ui.activities

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.example.quetzal.R
import com.example.quetzal.base.BaseActivity
import com.example.quetzal.databinding.ActivityMainBinding
import com.example.quetzal.models.screen.ActivityScreen
import com.example.quetzal.models.screen.Screen

class MainActivity : BaseActivity() {
    override val screen: Screen = ActivityScreen(false, "MainActivity")

    private lateinit var binding: ActivityMainBinding
    private var animationBtnExit: AnimationDrawable? = null
    private var animationBtnPlay: AnimationDrawable? = null
    private var animationBtnSettings: AnimationDrawable? = null

    override val viewBinding: ViewBinding
        get() {
            binding = ActivityMainBinding.inflate(layoutInflater)
            return binding
        }

    override fun prepareComponents() {
        //StarAnimation
        //Animation btnExit
        animationBtnExit = binding.btnExit.background as AnimationDrawable
        animationBtnExit!!.setEnterFadeDuration(1500)
        animationBtnExit!!.setExitFadeDuration(1500)
        //Animation btnPlay
        animationBtnPlay = binding.btnPlay.background as AnimationDrawable
        animationBtnPlay!!.setEnterFadeDuration(1500)
        animationBtnPlay!!.setExitFadeDuration(1500)
        //Animation btbSettings
        animationBtnSettings = binding.btSettings.background as AnimationDrawable
        animationBtnSettings!!.setEnterFadeDuration(1500)
        animationBtnSettings!!.setExitFadeDuration(1500)

        Glide.with(this)
            .load(R.raw.imagenamor)
            .into(binding.ivMenuPicture)

        binding.btnPlay.setOnClickListener {
            val mIntent = Intent(this@MainActivity, ContainerActivity::class.java)
            startActivity(mIntent)
            finish()
        }
        binding.btnExit.setOnClickListener { finish() }
    }

    override fun onResume() {
        super.onResume()
        if (animationBtnExit != null && !animationBtnExit!!.isRunning && animationBtnPlay != null && !animationBtnPlay!!.isRunning
            && animationBtnSettings != null && !animationBtnSettings!!.isRunning
        ) {
            animationBtnExit!!.start()
            animationBtnPlay!!.start()
            animationBtnSettings!!.start()
        }
    }

    override fun onPause() {
        super.onPause()
        if (animationBtnExit != null && animationBtnExit!!.isRunning && animationBtnPlay != null && animationBtnPlay!!.isRunning
            && animationBtnSettings != null && animationBtnSettings!!.isRunning
        ) {
            animationBtnExit!!.stop()
            animationBtnPlay!!.stop()
            animationBtnSettings!!.stop()
        }
    }
}