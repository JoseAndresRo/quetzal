package com.example.quetzal.ui.activities

import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.example.quetzal.ImageManager
import com.example.quetzal.ImageManager.Companion.imageManager
import com.example.quetzal.base.BaseActivity
import com.example.quetzal.databinding.ActivityContainerBinding
import com.example.quetzal.models.screen.ActivityScreen
import com.example.quetzal.models.screen.Screen
import com.example.quetzal.utils.Constants.ASK_1
import com.example.quetzal.utils.LogUtils
import com.example.quetzal.viewmodels.AskViewModel

class ContainerActivity : BaseActivity() {
    override var screen: Screen = ActivityScreen(false, "ContainerActivity")
    private lateinit var binding: ActivityContainerBinding

    override val viewBinding: ViewBinding
        get() {
            binding = ActivityContainerBinding.inflate(layoutInflater)
            return binding
        }

    override fun prepareComponents() {
        val viewModel: AskViewModel = ViewModelProvider(this).get(AskViewModel::class.java)
        viewModel.getAskById(ASK_1)
    }

    override fun onBackPressed() {}
}