package com.example.quetzal.ui.activities

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Intent
import android.media.MediaPlayer
import android.os.Handler
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.quetzal.R
import com.example.quetzal.base.BaseActivity
import com.example.quetzal.databinding.ActivitySplashBinding
import com.example.quetzal.models.screen.ActivityScreen
import com.example.quetzal.models.screen.Screen

class SplashActivity : BaseActivity() {
    override var screen: Screen = ActivityScreen(false, "Activity Splash")
    private lateinit var binding: ActivitySplashBinding
    private var charSequence: CharSequence? = null
    private var index = 0
    private val delay: Long = 200
    private val handler = Handler()

    override val viewBinding: ViewBinding
        get() {
                binding = ActivitySplashBinding.inflate(layoutInflater)
            return binding
        }

    override fun prepareComponents() {
        //Sound
        val mediaPlayer = MediaPlayer.create(this, R.raw.sonidocorazon)
        mediaPlayer.start()

        //Set full Screen
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        //Initialize top animation
        val animation1 = AnimationUtils.loadAnimation(this, R.anim.top_wave)
        //Start top animation
        binding.ivTop.animation = animation1

        //Initialize object animator
        val objectAnimator = ObjectAnimator.ofPropertyValuesHolder(
            binding.ivHeart,
            PropertyValuesHolder.ofFloat("scaleX", 1.2f),
            PropertyValuesHolder.ofFloat("scaleY", 1.2f)
        )

        //set duration
        objectAnimator.duration = 500
        //Set repeat count
        objectAnimator.repeatCount = ValueAnimator.INFINITE
        //Set repeat mode
        objectAnimator.repeatMode = ValueAnimator.REVERSE
        //Start animator
        objectAnimator.start()

        //Set animate text
        animateText("Plane of Love")

        //Load Gif Animation to heart beat
        Glide
            .with(this)
            .load(R.drawable.heart_beat)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.ivHeartBeat)

        //Initialize bottom animation
        val animation2 = AnimationUtils.loadAnimation(this, R.anim.bottom_wave)
        //Star bottom animation
        binding.ivBottom.animation = animation2
        Handler().postDelayed({
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            mediaPlayer.stop()
            finish()
        }, 4000)
    }

    private var runnable: Runnable = object : Runnable {
        override fun run() {
            //When runnable is run
            //Set text
            binding.textView.text = charSequence!!.subSequence(0, index++)
            //check condition
            if (index <= charSequence!!.length) {
                //When index is equal to text lengh
                //Run handler
                handler.postDelayed(this, delay)
            }
        }
    }

    //Create animated text method
    private fun animateText(cs: CharSequence?) {
        //Set text
        charSequence = cs
        //Clear index
        index = 0
        //Text clear
        binding.textView.text = ""
        //Remove call back
        handler.removeCallbacks(runnable)
        //Run handler
        handler.postDelayed(runnable, delay)
    }
}