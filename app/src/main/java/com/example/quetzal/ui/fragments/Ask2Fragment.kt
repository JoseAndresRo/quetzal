package com.example.quetzal.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.quetzal.base.NavigationFragment
import com.example.quetzal.databinding.FragmentAsk2Binding
import com.example.quetzal.models.Ask
import com.example.quetzal.models.screen.FragmentScreen
import com.example.quetzal.models.screen.Screen
import com.example.quetzal.utils.Constants.ASK_2
import com.example.quetzal.utils.LogUtils
import com.example.quetzal.viewmodels.AskViewModel

class Ask2Fragment : NavigationFragment() {
    override val screen: Screen = FragmentScreen(false, "Ask2")
    private var _binding: FragmentAsk2Binding? = null
    private val binding get()  = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentAsk2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun prepareComponents() {
        val viewModel = ViewModelProvider(this).get(AskViewModel::class.java)
        binding.btnNext.setOnClickListener { nextActivity() }

        viewModel.ask.observe(this, { ask: Ask? ->
            if (ask != null) {
                binding.txQuestion2.text = ask.askName
                LogUtils.print("AskInformation : " + ask.askName)
            }
        })
        viewModel.getAskById(ASK_2)
    }

    private fun nextActivity() {
        val option1 = binding.etName1.text.toString()
        val option2 = binding.etName2.text.toString()
        when {
            option1.isEmpty() -> {
                showToast("option 1 is empty")
            }
            option2.isEmpty() -> {
                showToast("option 2 is empty")
            }
            else -> {
                openPunishment2(option1, option2, binding)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}