package com.example.quetzal.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.quetzal.utils.Constants.CATEGORY_A
import com.example.quetzal.utils.Constants.CATEGORY_B
import com.example.quetzal.utils.Constants.CATEGORY_C
import com.example.quetzal.utils.LogUtils.Companion.print
import java.util.*

/**
 * Created by Andrés Rodríguez on 15/10/2020.
 */
class ResultRepository private constructor(application: Application) {
    private val result1 = MutableLiveData<String>()
    private val result2 = MutableLiveData<String>()
    private val result3 = MutableLiveData<String>()
    fun getResult1(): LiveData<String> {
        return result1
    }

    fun getResult2(): LiveData<String> {
        return result2
    }

    fun getResult3(): LiveData<String> {
        return result3
    }

    private fun setResult1(result1: String) {
        this.result1.value = result1
        this.result1.postValue(result1)
    }

    private fun setResult2(result2: String) {
        this.result2.value = result2
        this.result2.postValue(result2)
    }

    private fun setResult3(result3: String) {
        this.result3.value = result3
        this.result3.postValue(result3)
    }

    fun generalResult(option1: String, option2: String, option3: String, category: String?) {
        print("3OPTIONS_RECEIVED: $option1$option2$option3")
        val result: String
        when (category) {
            CATEGORY_A -> {
                result = calculateResult(option1, option2, option3)
                setResult1(result)
                print("DATA_FROM_GENERAL_RESULT: $result")
            }
            CATEGORY_B -> {
                result = calculateResult(option1, option2, option3)
                setResult2(result)
                print("DATA_FROM_GENERAL_RESULT: $result")
            }
            CATEGORY_C -> {
                result = calculateResult(option1, option2, option3)
                setResult3(result)
                print("DATA_FROM_GENERAL_RESULT $result")
            }
        }
    }

    private fun calculateResult(option1: String, option2: String, option3: String): String {
        val random = Random()
        when (random.nextInt(3)) {
            0 -> {
                print("The option :$option1 Was Added to Result")
                return option1
            }
            1 -> {
                print("The option :$option2 Was Added to Result")
                return option2
            }
            2 -> {
                print("The option :$option3 Was Added to Result")
                return option3
            }
            else -> {
                print("Error option no exist")
            }
        }
        return "Error"
    }

    companion object {
        @Volatile
        var instance: ResultRepository? = null
        fun getInstance(application: Application): ResultRepository? {
            if (instance == null) {
                synchronized(ResultRepository::class.java) {
                    if (instance == null) {
                        instance = ResultRepository(application)
                    }
                }
            }
            return instance
        }
    }
}