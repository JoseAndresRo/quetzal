package com.example.quetzal.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.quetzal.asynctasks.GetRandomPunishmentByCategory
import com.example.quetzal.interfaces.RoomListener
import com.example.quetzal.models.Punishment
import com.example.quetzal.room.AppDatabase.Companion.getInstance
import com.example.quetzal.room.dao.PunishmentDao
import com.example.quetzal.utils.LogUtils.Companion.print

class PunishmentRepository private constructor(application: Application) {
    private val punishmentDao: PunishmentDao
    private val punishment = MutableLiveData<Punishment>()

    fun getPunishment(): LiveData<Punishment> {
        return punishment
    }

    private fun setPunishment(punishment: Punishment?) {
        this.punishment.value = punishment
        this.punishment.postValue(null)
    }

    fun getRandomPunishmentByCategory(category: String) {
        print("CATEGORY_ENTERED: $category")
        GetRandomPunishmentByCategory(punishmentDao, category) { result ->
            setPunishment(result)
            print("RANDOM_PUNISHMENT: " + result.punishmentId + " " + result.punishmentName)
        }.execute()
    }

    companion object {
        @Volatile
        var instance: PunishmentRepository? = null
        @JvmStatic
        fun getInstance(application: Application): PunishmentRepository? {
            if (instance == null) {
                synchronized(PunishmentRepository::class.java) {
                    if (instance == null) {
                        instance = PunishmentRepository(application)
                    }
                }
            }
            return instance
        }
    }

    init {
        val database = getInstance(application.applicationContext)
        punishmentDao = database.punishmentDao()
        punishment.value = null
    }
}