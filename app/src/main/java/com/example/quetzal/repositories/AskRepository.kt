package com.example.quetzal.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.quetzal.asynctasks.GetAsksByIdAsyncTask
import com.example.quetzal.models.Ask
import com.example.quetzal.room.AppDatabase.Companion.getInstance
import com.example.quetzal.room.dao.AskDao
import com.example.quetzal.utils.LogUtils.Companion.print

class AskRepository private constructor(application: Application) {
    private val askDao: AskDao
    private val asks: LiveData<List<Ask>>
    val ask = MutableLiveData<Ask>()

    fun getAsk(): LiveData<Ask> {
        return ask
    }

    private fun setAsk(ask: Ask) {
        this.ask.value = ask
        this.ask.postValue(null)
    }

    fun getAsksById(askId: Int) {
        GetAsksByIdAsyncTask(askDao, askId) { ask ->
            setAsk(ask)
            print("AskId: " + ask.askId)
        }.execute()
    }

    fun loadAsks(): LiveData<List<Ask>> {
        return asks
    }

    companion object {
        @Volatile
        var instance: AskRepository? = null
        fun getInstance(application: Application): AskRepository? {
            if (instance == null) {
                synchronized(AskRepository::class.java) {
                    if (instance == null) {
                        instance = AskRepository(application)
                    }
                }
            }
            return instance
        }
    }

    init {
        val database = getInstance(application.applicationContext)
        askDao = database.askDao()
        asks = askDao.allAsks()
        ask.value = null
    }
}