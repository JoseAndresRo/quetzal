package com.example.quetzal.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.quetzal.repositories.ResultRepository

/**
 * Created by Andrés Rodríguez on 15/10/2020.
 */
class ResultViewModel(application: Application?) : AndroidViewModel(application!!) {
    private val repository: ResultRepository? = ResultRepository.getInstance(application!!)
    val result1: LiveData<String> = repository!!.getResult1()
    val result2: LiveData<String> = repository!!.getResult2()
    val result3: LiveData<String> = repository!!.getResult3()

    fun generalResult(option1: String?, option2: String?, option3: String?, category: String?) {
        repository!!.generalResult(option1!!, option2!!, option3!!, category)
    }
}