package com.example.quetzal.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.quetzal.repositories.AskRepository
import androidx.lifecycle.LiveData
import com.example.quetzal.models.Ask

class AskViewModel(application: Application?) : AndroidViewModel(application!!) {

    private val repository: AskRepository? = AskRepository.getInstance(application!!)
    val ask: LiveData<Ask> = repository!!.ask
    private val asks: LiveData<List<Ask>> = repository!!.loadAsks()
    private val askCategory: LiveData<String>? = null

    fun getAskById(id: Int) {
        repository!!.getAsksById(id)
    }

    fun loadAsks(): LiveData<List<Ask>> {
        return asks
    }

}