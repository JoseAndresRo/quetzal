package com.example.quetzal.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.quetzal.models.Punishment
import com.example.quetzal.repositories.PunishmentRepository
import com.example.quetzal.repositories.PunishmentRepository.Companion.getInstance

class PunishmentViewModel(application: Application?) : AndroidViewModel(application!!) {
    private val repository: PunishmentRepository? = getInstance(application!!)
    val punishment: LiveData<Punishment> = repository!!.getPunishment()

    fun getPunishmentCategory(category: String?) {
        repository!!.getRandomPunishmentByCategory(category!!)
    }
}