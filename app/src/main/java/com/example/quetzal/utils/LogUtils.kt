package com.example.quetzal.utils

import android.util.Log
import com.example.quetzal.BuildConfig
import com.example.quetzal.ImageManager
import java.lang.Exception

class LogUtils {
    companion object {
        fun print(text: String?) {
            if (BuildConfig.DEBUG) {
                if (text != null && text.isNotEmpty()) {
                    Log.d("Andy", text)
                }
            }

        }

        fun print(TAG: String?, text: String?) {
            if (BuildConfig.DEBUG) {
                if (text != null && text.isNotEmpty()) {
                    Log.d(TAG, text)
                }
            }
        }

        fun print(TAG: String, ex: Exception) {
            if (BuildConfig.DEBUG) {
                Log.d("Andy", "$TAG -> $ex")
            }
        }
    }
}