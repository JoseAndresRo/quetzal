package com.example.quetzal.utils

object Constants {
    // Questions
    const val ASK_1 = 1
    const val ASK_2 = 2
    const val ASK_3 = 3
    const val ASK_4 = 4

    // Punishment categories
    const val CATEGORY_A = "A"
    const val CATEGORY_B = "B"
    const val CATEGORY_C = "C"
    const val CATEGORY_D = "D"

    // Bundle
    const val OPTION_1 = "OPTION1"
    const val OPTION_2 = "OPTION2"
}