package com.example.quetzal.asynctasks;

import android.os.AsyncTask;

import androidx.annotation.NonNull;

import com.example.quetzal.interfaces.RoomListener;
import com.example.quetzal.models.Ask;
import com.example.quetzal.room.dao.AskDao;

public class GetAsksByIdAsyncTask extends AsyncTask<Void, Void, Ask> {
    private final AskDao askDao;

    private final int askId;

    private final RoomListener<Ask> listener;

    public GetAsksByIdAsyncTask(@NonNull AskDao askDao, int askId, @NonNull RoomListener<Ask> listener) {
        this.askDao = askDao;
        this.askId = askId;
        this.listener = listener;
    }

    @Override
    protected Ask doInBackground(Void... voids) {
        return askDao.getAskById(askId);
    }

    @Override
    protected void onPostExecute(Ask ask) {
       if (ask != null) {
           listener.onFinish(ask);
       }
    }
}
