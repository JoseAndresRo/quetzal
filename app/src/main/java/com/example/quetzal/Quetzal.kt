package com.example.quetzal

import android.app.Application
import com.example.quetzal.ImageManager.Companion.imageManager

/**
 * Created by Andrés Rodríguez on 06/11/2020.
 */
class Quetzal : Application() {
    override fun onCreate() {
        super.onCreate()
        //Application Class
        imageManager.initialize(this)
    }
}